import unittest
from selenium import webdriver
import HtmlTestRunner
import BaseTest
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir+'\pages')

import BasePage
import HomePage
import CheckboxPage

class CheckboxTest(BaseTest.SetUpTearDown):

    def test_cycle_checkbox(self):
        

        base_page = BasePage.MainPage(self.driver)
        base_page.go_to_url()

        home_page = HomePage.HomePageFunctions(self.driver)
        home_page.click_checkbox_link()
        
        checkbox_page = CheckboxPage.CheckboxPageFunctions(self.driver)
        checkbox_page.click_checkbox_option(1)
        checkbox_page.click_checkbox_option(2)
        checkbox_page.click_checkbox_option(1)
        checkbox_page.click_checkbox_option(2)


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = parentdir + '\\reports\\CHECKBOX'))