import unittest
from selenium import webdriver
import HtmlTestRunner
import BaseTest

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir+'\pages')

import BasePage
import HomePage
import DropdownPage

class DropdownTest(BaseTest.SetUpTearDown):

    def test_dropdown(self):
        
        base_page = BasePage.MainPage(self.driver)
        base_page.go_to_url()

        home_page = HomePage.HomePageFunctions(self.driver)
        home_page.click_dropdown_link()

        dropdown_page = DropdownPage.DropdownPageFunctions(self.driver)
        
        # selects option 1 from dropdown
        dropdown_page.click_dropdown()
        dropdown_page.click_dropdown_option(1)

        # selects option 2 from dropdown
        dropdown_page.click_dropdown()
        dropdown_page.click_dropdown_option(2)


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = parentdir + '\\reports\\DROPDOWN'))