import unittest
from selenium import webdriver
import HtmlTestRunner
import BaseTest
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir+'\pages')

import BasePage
import HomePage
import EntryAdPage

class CloseEntryAdTest(BaseTest.SetUpTearDown):

    def test_close_ad(self):
        
        base_page = BasePage.MainPage(self.driver)
        base_page.go_to_url()

        home_page = HomePage.HomePageFunctions(self.driver)
        home_page.click_entry_ad_link()

        entry_ad_page = EntryAdPage.EntryAdPageFunctions(self.driver)
        
        entry_ad_page.click_close()


if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = parentdir + '\\reports\\CLOSE'))