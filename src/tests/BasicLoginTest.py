import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert
import HtmlTestRunner
import BaseTest
import time

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir+'\pages')

import BasePage
import HomePage
import DropdownPage

class BasicLoginTest(BaseTest.SetUpTearDown):
# NOT WORKING
    def test_login(self):

        driver = self.driver
        
        base_page = BasePage.MainPage(self.driver)
        base_page.go_to_url()

        home_page = HomePage.HomePageFunctions(self.driver)
        # home_page.click_basic_authentication_link()

        # driver.get('http://the-internet.herokuapp.com/basic_auth')
        driver.get('http://admin:admin@the-internet.herokuapp.com/basic_auth')

        time.sleep(4)

        # driver.switch_to.alert.send_keys(Keys.TAB)
        # driver.switch_to.alert.send_keys('admin')
        # driver.switch_to.alert.send_keys(Keys.RETURN)



if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = parentdir + '\\reports\\LOGIN'))