import unittest
from selenium import webdriver
import HtmlTestRunner
import BaseTest

import os, sys, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir+'\pages')

import BasePage
import HomePage
import JSAlertsPage

class JSAlertsTest(BaseTest.SetUpTearDown):

    def test_javascript_elements(self):
        driver = self.driver

        base_page = BasePage.MainPage(self.driver)
        base_page.go_to_url()

        home_page = HomePage.HomePageFunctions(self.driver)
        home_page.click_jsalerts()

        js_alerts_page = JSAlertsPage.JSAlertsPageFunctions(self.driver)
        js_alerts_page.click_jsconfirm()

        driver.switch_to.alert.accept()

        self.assertEqual(js_alerts_page.get_jsresult_text(), "You clicked: Ok")

if __name__ == "__main__":
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = parentdir + '\\reports\\JSALERTS'))