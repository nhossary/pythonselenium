from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import BasePage

class EntryAdPageLocators(object):
    CLOSE = (By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/div[3]/p')

class EntryAdPageFunctions(BasePage.BasePage):

    def click_close(self):
        driver = self.driver
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[2]/div/div[2]/div[2]/div[3]/p')))
        self.driver.find_element(*EntryAdPageLocators.CLOSE).click()

