from selenium.webdriver.common.by import By
import BasePage

class HomePageLocators(object):
    AB_TESTING = (By.XPATH, '/html/body/div[2]/div/ul/li[1]/a')
    ADD_REMOVE_ELEMENTS = (By.XPATH, '/html/body/div[2]/div/ul/li[2]/a')
    BASIC_AUTHENTICATION = (By.XPATH, '/html/body/div[2]/div/ul/li[3]/a')
    BROKEN_IMAGES = (By.XPATH, '/html/body/div[2]/div/ul/li[4]/a')
    CHALLENGING_DOM = (By.XPATH, '/html/body/div[2]/div/ul/li[5]/a')
    CHECKBOX_LINK = (By.XPATH, '/html/body/div[2]/div/ul/li[6]/a')
    CONTEXT_MENU = (By.XPATH, '/html/body/div[2]/div/ul/li[7]/a')
    DIGEST_AUTHENTICATION = (By.XPATH, '/html/body/div[2]/div/ul/li[8]/a')
    DISAPPEARING_ELEMENTS = (By.XPATH, '/html/body/div[2]/div/ul/li[9]/a')
    DRAG_AND_DROP = (By.XPATH, '/html/body/div[2]/div/ul/li[10]/a')
    DROPDOWN_LINK = (By.XPATH, '/html/body/div[2]/div/ul/li[11]/a')
    DYNAMIC_CONTENT = (By.XPATH, '/html/body/div[2]/div/ul/li[12]/a')
    DYNAMIC_CONTROLS = (By.XPATH, '/html/body/div[2]/div/ul/li[13]/a')
    DYNAMIC_LOADING = (By.XPATH, '/html/body/div[2]/div/ul/li[14]/a')
    ENTRY_AD = (By.XPATH, '/html/body/div[2]/div/ul/li[15]/a')
    EXIT_INTENT = (By.XPATH, '/html/body/div[2]/div/ul/li[16]/a')
    FILE_DOWNLOAD = (By.XPATH, '/html/body/div[2]/div/ul/li[17]/a')
    FILE_UPLOAD = (By.XPATH, '/html/body/div[2]/div/ul/li[18]/a')
    FLOATING_MENU = (By.XPATH, '/html/body/div[2]/div/ul/li[19]/a')
    FORGOT_PASSWORD = (By.XPATH, '/html/body/div[2]/div/ul/li[20]/a')
    FORM_AUTHENTICATION = (By.XPATH, '/html/body/div[2]/div/ul/li[21]/a')
    FRAMES = (By.XPATH, '/html/body/div[2]/div/ul/li[22]/a')
    GEOLOCATION = (By.XPATH, '/html/body/div[2]/div/ul/li[23]/a')
    HORIZONTAL_SLIDER = (By.XPATH, '/html/body/div[2]/div/ul/li[24]/a')
    HOVERS = (By.XPATH, '/html/body/div[2]/div/ul/li[25]/a')
    INFINITE_SROLL = (By.XPATH, '/html/body/div[2]/div/ul/li[26]/a')
    INPUTS = (By.XPATH, '/html/body/div[2]/div/ul/li[27]/a')
    JQUERY_UI_MENUS = (By.XPATH, '/html/body/div[2]/div/ul/li[28]/a')
    JS_ALERTS = (By.XPATH, '/html/body/div[2]/div/ul/li[29]/a')
    JS_ONLOAD_EVENT_ERROR = (By.XPATH, '/html/body/div[2]/div/ul/li[30]/a')
    KEY_PRESSES = (By.XPATH, '/html/body/div[2]/div/ul/li[31]/a')
    LARGE_AND_DEEP_DOM = (By.XPATH, '/html/body/div[2]/div/ul/li[32]/a')
    MULTIPLE_WINDOWS = (By.XPATH, '/html/body/div[2]/div/ul/li[33]/a')
    NESTED_FRAMES = (By.XPATH, '/html/body/div[2]/div/ul/li[34]/a')
    NOTIFICATION_MESSAGES = (By.XPATH, '/html/body/div[2]/div/ul/li[35]/a')
    REDIRECT_LINK = (By.XPATH, '/html/body/div[2]/div/ul/li[36]/a')
    SECURE_FILE_DOWNLOAD = (By.XPATH, '/html/body/div[2]/div/ul/li[37]/a')
    SHIFTING_CONTENT = (By.XPATH, '/html/body/div[2]/div/ul/li[38]/a')
    SLOW_RESOURCES = (By.XPATH, '/html/body/div[2]/div/ul/li[39]/a')
    SORTABLE_DATA_TABLES = (By.XPATH, '/html/body/div[2]/div/ul/li[40]/a')
    STATUS_CODES = (By.XPATH, '/html/body/div[2]/div/ul/li[41]/a')
    TYPOS = (By.XPATH, '/html/body/div[2]/div/ul/li[42]/a')
    WYSIWYG_EDITOR = (By.XPATH, '/html/body/div[2]/div/ul/li[43]/a')

class HomePageFunctions(BasePage.BasePage):

    def click_jsalerts(self):
        element = self.driver.find_element(*HomePageLocators.JS_ALERTS)
        element.click()

    def click_dropdown_link(self):
        element = self.driver.find_element(*HomePageLocators.DROPDOWN_LINK)
        element.click()

    def click_digest_authentication_link(self):
        element = self.driver.find_element(*HomePageLocators.DIGEST_AUTHENTICATION)
        element.click()

    def click_basic_authentication_link(self):
        element = self.driver.find_element(*HomePageLocators.BASIC_AUTHENTICATION)
        element.click()

    def click_entry_ad_link(self):
        element = self.driver.find_element(*HomePageLocators.ENTRY_AD)
        element.click()

    def click_checkbox_link(self):
        element = self.driver.find_element(*HomePageLocators.CHECKBOX_LINK)
        element.click()
