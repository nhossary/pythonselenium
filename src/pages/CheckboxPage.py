from selenium.webdriver.common.by import By
import BasePage

class CheckboxPageLocators(object):
    DROPDOWN = (By.ID, 'dropdown')
    CHECKBOX_ONE = (By.XPATH, '/html/body/div[2]/div/div/form/input[1]')
    CHECKBOX_TWO = (By.XPATH, '/html/body/div[2]/div/div/form/input[2]')

class CheckboxPageFunctions(BasePage.BasePage):

    def click_checkbox_option(self, option):
        if(option == 1):
            element = self.driver.find_element(*CheckboxPageLocators.CHECKBOX_ONE)
            element.click()
        elif(option == 2):
            element = self.driver.find_element(*CheckboxPageLocators.CHECKBOX_TWO)
            element.click()

