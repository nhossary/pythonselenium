from selenium.webdriver.common.by import By
import BasePage

class JSAlertsPageLocators(object):
    JS_CONFIRM = (By.XPATH, '/html/body/div[2]/div/div/ul/li[2]/button')
    JS_RESULT = (By.ID, 'result')

class JSAlertsPageFunctions(BasePage.BasePage):

    def click_jsconfirm(self):
        element = self.driver.find_element(*JSAlertsPageLocators.JS_CONFIRM)
        element.click()
    
    def get_jsresult_text(self):
        js_result = self.driver.find_element(*JSAlertsPageLocators.JS_RESULT).text
        return js_result

