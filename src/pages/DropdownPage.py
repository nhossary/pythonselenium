from selenium.webdriver.common.by import By
import BasePage

class DropdownPageLocators(object):
    DROPDOWN = (By.ID, 'dropdown')
    OPTION_ONE = (By.XPATH, '/html/body/div[2]/div/div/select/option[2]')
    OPTION_TWO = (By.XPATH, '/html/body/div[2]/div/div/select/option[3]')

class DropdownPageFunctions(BasePage.BasePage):

    def click_dropdown(self):
        element = self.driver.find_element(*DropdownPageLocators.DROPDOWN)
        element.click()

    def click_dropdown_option(self, option):
        if(option == 1):
            element = self.driver.find_element(*DropdownPageLocators.OPTION_ONE)
            element.click()
        elif(option == 2):
            element = self.driver.find_element(*DropdownPageLocators.OPTION_TWO)
            element.click()

